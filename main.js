import Component from './src/Component.js';
import Router from './src/Router.js';
// eslint-disable-next-line import/extensions,no-unused-vars
import * as ObjectHelper from './src/Helper/ObjectHelper.js';
// eslint-disable-next-line import/extensions,no-unused-vars
import * as StringHelper from './src/Helper/StringHelper.js';

export default class TinyComp extends Component {
  constructor(router, container) {
    const containerProps = [];
    for (let i = 0; i < container.attributes.length; i += 1) {
      containerProps[container.attributes[i].nodeName] = container.attributes[i].nodeValue;
    }
    super(container.tagName, containerProps, null);

    if (!ObjectHelper.default.typeCheck(router, { type: 'object' })) {
      throw new Error('TinyComp : Router need to be a Router instance !');
    }

    this.router = router;
    this.state = container;

    this.router.setApp(this);
    this.router.updateByUri();
    this.router.launchListners();
  }

  createElement(type, props, ...children) {
    // Todo: Type verifications
    const newComp = new Component(type, props, this, ...children);
    this.props.children.push(newComp);
    return newComp;
  }

  display() {
    this._render();
  }

  _render() {
    this.props.children.forEach((child) => {
      child._render(this.state);
    });
  }
}

export {
  Router as TinyRouter,
  TinyComp,
};
