// eslint-disable-next-line no-extend-native,func-names
Object.prototype.prop_access = function (attribute) {
  if (typeof attribute !== 'string' || attribute.length === 0 || attribute === null) return this;
  const attributeStop = [];
  const valueObjAttribute = attribute.split('.').reduce((prev, next) => {
    if (prev) {
      if (attributeStop.length === 0) {
        attributeStop.push(next);
      } else {
        attributeStop.push(`.${next}`);
      }
    }
    return prev && prev[next];
  }, this);
  if (valueObjAttribute === undefined) {
    return attributeStop.join('');
  }
  return valueObjAttribute;
};

function baseTypeCheck(v, t) {
  if (typeof t !== 'string' || t.length === 0) return false;
  if (t === 'null') {
    return v === null;
  }
  if (t === 'array') {
    return Array.isArray(v);
  }
  if (v === null || v instanceof Array) {
    return false;
  }
  return typeof v === t;
}

function typeCheck(v, conf) {
  if (Object.prototype.hasOwnProperty.call(conf, 'type')) {
    if (!baseTypeCheck(v, conf.type)) return false;
  }
  if (Object.prototype.hasOwnProperty.call(conf, 'value')) {
    if (JSON.stringify(v) !== JSON.stringify(conf.value)) return false;
  }
  if (Object.prototype.hasOwnProperty.call(conf, 'enum')) {
    if (!conf.enum.find((value) => JSON.stringify(value) === JSON.stringify(v))) return false;
  }

  return true;
}

export default {
  typeCheck,
};
