// eslint-disable-next-line no-extend-native,func-names
String.prototype.interpolate = function (obj) {
  return this.replace(/\{\{\s*(.*?)\s*\}\}/g, (groupe1, patternToReplace) => obj.prop_access(patternToReplace));
};
